#include "list.h"

/* pointer-ul dublu ** dat ca parametru echivaleaza cu posibilitatea de a trimite ca parametru un pointer simplu si a returna variabila head */
int create(node_t** head, char info) {
	*head = (node_t*) malloc(sizeof(node_t));
	
	if(*head == NULL) {
		return 1;
	}
	
	(*head)->val = info;
	(*head)->next = NULL;
	
	return 0;
}

int push_back(node_t** head, char info) {
	int err_code = 0; /* variabila care retine codul de eroare astfel: 0-program functioneaza corect; 1-problema la alocarea memporiei; 2-problema la depasirea dimensiunii listei */ 
	
	if(*head == NULL) {
		err_code = create(head, info);
		return err_code;
	}
	
	node_t* tmp = *head;
	
	while(tmp->next != NULL) {
		tmp = tmp->next;
	}
	
	node_t* crt;
	crt = (node_t*) malloc(sizeof(node_t));
	
	if(crt == NULL) {
		err_code = 1;
		return err_code;
	}
	
	crt->val = info;
	crt->next = NULL;
	tmp->next = crt;
	
	return err_code;
}

/* functia va scoate ultimul element din lista */
int pop_back(node_t** head, char* info) {
	int err_code = 0;
	
	if(*head == NULL) {
		err_code = 2;
		return err_code;
	}
	
	node_t* tmp = *head;
	
	while(tmp->next->next != NULL) {
		tmp = tmp->next;
	}
	
	*info = tmp->next->val;
	free(tmp->next);
	tmp->next = NULL;
	
	return err_code;
}

/* adaugare la inceputul listei */
int push(node_t** head, char info) {
	int err_code = 0;
	
	if(*head == NULL) {
		err_code = create(head, info);
		return err_code;
	}
	
	node_t* crt;
	crt = (node_t*) malloc(sizeof(node_t));
	
	if(crt == NULL) {
		err_code = 1;
		return err_code;
	}
	
	crt->val = info;
	crt->next = *head;
	*head = crt;
	
	return err_code;
}

/* functia va scoate primul element din lista */
int pop(node_t** head, char* info) {
	if(*head == NULL) {
		return 1;
	}
	
	node_t* tmp = *head;
	(*head) = (*head)->next;
	*info = tmp->val;
	free(tmp);
	
	return 0;
}

/* se presupune ca avem deja o lista creata */
int push_at_index(node_t** head, char info, int index) {
	node_t* tmp = *head;
	int contor = 0; /* variabila care va retine pozitia elementului curent */
	int err_code = 0;
	
	if(index == 0) {
		err_code = push(head, info);
		return err_code;
	}
	
	while(contor < (index-1)) {
		if(tmp == NULL) {	/* daca se depaseste dimensiunea listei */
			err_code = 2;
			return err_code;
		}
		tmp = tmp->next;
		contor++;
	}
	
	node_t* crt;
	crt = (node_t*) malloc(sizeof(node_t));
	
	if(crt == NULL) {
		err_code = 1;
		return err_code;
	}
	
	crt->val = info;
	crt->next = tmp->next;
	tmp->next = crt;
	
	return err_code;
	
}
/* functia va scoate din lista elementul de la pozitia specificata */
int pop_at_index(node_t** head, char* info, int index) {
	node_t* tmp = *head;
	int contor = 0; 
	int err_code = 0;
	
	if(index == 0) {
		err_code = pop(head, info);
		return err_code;
	}
	
	while(contor < (index-1)) {
		if(tmp == NULL) {
			err_code = 2;
			return err_code;
		}
		tmp = tmp->next;
		contor++;
	}
	
	node_t* tmp2 = tmp->next;
	tmp->next = tmp->next->next;
	*info = tmp2->val;
	free(tmp2);
	
	return err_code;
}

void print_list(node_t* head) {
/* daca lista este goala se va afisa un mesaj */
	if(head == NULL) {
		printf("Lista goala!");
	}
	
	while(head != NULL){
		printf("%c ",head->val);
		head = head->next;
	}
	printf("\n");
	
}

void delete_list(node_t** head) {
	if(*head == NULL) {
		return;
	}
	
	node_t* tmp = *head;
	
	while(*head != NULL) {
		*head = (*head)->next;
		free(tmp);
		tmp = *head;
	}
	
}















