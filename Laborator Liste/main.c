#include "list.h"

int main() {
	int result;
	char popped_elem;
	node_t** head = (node_t**) malloc(sizeof(node_t*));
	*head = NULL;
	
	result = create(head, 'a');
	result = push(head, 'b');
	result = push_back(head, 'c');
	result = push_at_index(head, 'd', 2);
	
	print_list(*head);
	result = pop_at_index(head, &popped_elem, 0);
	printf("Popped element: %c\n", popped_elem);
	result = pop(head, &popped_elem);
	printf("Popped element: %c\n", popped_elem);
	print_list(*head);
	delete_list(head); /* se goleste intreaga lista */
	print_list(*head); /* se verifica afisarea pentru o lista goala */
	
	free(head);
	
	return 0;
}
