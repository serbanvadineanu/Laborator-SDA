#ifndef HEADER_M
#define HEADER_M

#include<stdio.h>
#include<stdlib.h>

typedef struct node{
	char val;
	struct node* next;
}node_t;

int create(node_t**, char);
int push_back(node_t**, char);
int push(node_t**, char);
int push_at_index(node_t**, char, int);
int pop_at_index(node_t**, char*, int);
int pop(node_t**, char*);
int pop_back(node_t**, char*);
void print_list(node_t*);
void delete_list(node_t**); 

#endif
